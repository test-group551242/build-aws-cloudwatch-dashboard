#creating VPC with two AZ (1 private & 1 public subnet each)

module "my-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.2.0"

  name ="my-vpc"
  cidr = var.vpc_cidr

  private_subnets = var.private_subnets_cidr
  private_subnet_names = var.private_subnet_names
  
  public_subnets = var.public_subnets_cidr
  public_subnet_names = var.public_subnet_names

  azs = ["us-east-1a", "us-east-1b"]

  enable_nat_gateway = true
  single_nat_gateway = true  #all privates subnets will route their internet traffic through this single NAT GW
  enable_dns_hostnames = true  #when EC2 is launched it will get a public & private dns names
  
  map_public_ip_on_launch = false 

}


#security group for my load balancer 

resource "aws_security_group" "sg-alb" {
  name   = "SG_ALB"
  vpc_id = module.my-vpc.vpc_id
  
  ingress {
    description      = "Allow http request from anywhere"
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = ["0.0.0.0/0"]
  }
  
  ingress {
    description      = "Allow https request from anywhere"
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "Allow ssh request from anywhere"
    protocol         = "tcp"
    from_port        = 22
    to_port          = 22
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "sg_alb"
  }

}

#security group of my auto scaling group

resource "aws_security_group" "sg-asg" {
  name   = "sg_asg"
  vpc_id = module.my-vpc.vpc_id

  ingress {
    description     = "Allow http request from Load Balancer"
    protocol        = "tcp"
    from_port       = 80 # range of
    to_port         = 80 # port numbers
    security_groups = [aws_security_group.sg-alb.id]
  }
  
  ingress {
    description     = "Allow ssh requests"
    protocol        = "tcp"
    from_port       = 22 
    to_port         = 22 
    security_groups = [aws_ec2_instance_connect_endpoint.ec2_enpoint.id]
  } 

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#instance connect endpoint 

resource "aws_ec2_instance_connect_endpoint" "ec2_enpoint" {
  subnet_id = "${element(module.my-vpc.private_subnets, 0)}"  #specufy any private subnet
}

resource "aws_security_group" "sg-endpoint" {
  name   = "sg_endpoint"
  vpc_id = module.my-vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "22"
    cidr_blocks = [aws_security_group.sg-asg.id]
  }

  tags = {
    Name = "sg_ec2_endpoint"
  }
}

