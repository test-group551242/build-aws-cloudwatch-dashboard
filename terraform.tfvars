vpc_cidr = "10.10.0.0/16"
private_subnets_cidr = ["10.10.1.0/24","10.10.3.0/24"]
public_subnets_cidr = ["10.10.2.0/24","10.10.4.0/24"]
private_subnet_names = ["my-private-subnet"]
public_subnet_names = ["my-public-subnet"]
ami_ec2 = "ami-079db87dc4c10ac91"

#ec2-instance= "i-03e485bc335b9a58a"
