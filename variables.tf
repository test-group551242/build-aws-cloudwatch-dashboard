variable "vpc_cidr" {}
variable "private_subnets_cidr" {}
variable "public_subnets_cidr" {}
variable "private_subnet_names" {}
variable "public_subnet_names" {}
variable "ami_ec2" {}


variable "ec2-instance" {
  type = string
  default = "i-03e485bc335b9a58a"
}