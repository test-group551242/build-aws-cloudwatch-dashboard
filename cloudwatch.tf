
resource "aws_cloudwatch_dashboard" "my-dashboard" {
  dashboard_name = "my-dashboard"

  dashboard_body = jsonencode({
    widgets = [
      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/EC2",
              "CPUUtilization",
              "InstanceId",
              "${aws_autoscaling_group.my-asg.id}"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "${aws_autoscaling_group.my-asg.id} - CPU Utilization"
        }
      },
      {
        type   = "text"
        x      = 0
        y      = 3
        width  = 12
        height = 2

        properties = {
          markdown = "My Demo Dashboard"
        }
      },
      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/EC2",
              "NetworkIn",
              "InstanceId",
              "${aws_autoscaling_group.my-asg.id}"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "${aws_autoscaling_group.my-asg.id} - NetworkIn"
        }
      }
    ]
  })
}

/*

resource "aws_cloudwatch_metric_alarm" "ec2-cpu-alarm" {
  alarm_name = "ec2-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = 2
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = 120 
  statistic = "Average"
  threshold = 80 
  alarm_description = "this metric monitors ec2 cpu utilisation reaches 80%"
  insufficient_data_actions = []
  dimensions = {
    InstanceId = module.ec2_instance_1.id
  }
  alarm_actions = [ aws_sns_topic.ec2-cloudwatch-alarm.arn ]
}

resource "aws_cloudwatch_metric_alarm" "asg_alarm" {
  alarm_name = "asg_alarm"
  comparison_operator = "LessThanThreshold"
  evaluation_periods = 2
  metric_name = "StatusCheckFailed_Instance" 
  namespace = "AWS/EC2"
  period = 120 #in seconds 
  statistic = "Average"
  threshold = 1
  alarm_description = "this metric reports whether the instance has passed the instance status check in the last minute."
  insufficient_data_actions = []
  dimensions = {
    InstanceId = aws_autoscaling_group.my-asg.id
  }
  alarm_actions = [ aws_sns_topic.ec2-cloudwatch-alarm.arn ]
}

*/
