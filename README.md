# build-aws-cloudwatch-dashboard

## What is AWS CloudWatch?

Amazon CloudWatch is a centralized monitoring service that offers a unified view of your AWS resources and applications. It provides a set of tools for collecting, analyzing, and visualizing data from various sources in real time. Amazon CloudWatch collects log and metric data from EC2 instances and other AWS services. CloudWatch includes a dashboard feature for viewing metrics and alarms to create notifications and perform actions when breaching an alarm threshold or when the alarm condition resets. CloudWatch is the AWS-provided observability platform.

## Metric information

- available metric_name 

https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/aws-services-cloudwatch-metrics.html


- NetworkIn

The number of bytes received by the instance on all network interfaces. This metric identifies the volume of incoming network traffic to a single instance. The number reported is the number of bytes received during the period. If you are using basic (5-minute) monitoring and the statistic is Sum, you can divide this number by 300 to find Bytes/second. If you have detailed (1-minute) monitoring and the statistic is Sum, divide it by 60. You can also use the CloudWatch metric math function DIFF_TIME to find the bytes per second. For example, if you have graphed NetworkIn in CloudWatch as m1, the metric math formula m1/(DIFF_TIME(m1)) returns the metric in bytes/second. 

- CPUUtilization

The percentage of physical CPU time that Amazon EC2 uses to run the EC2 instance, which includes time spent to run both the user code and the Amazon EC2 code. At a very high level, CPUUtilization is the sum of guest CPUUtilization and hypervisor CPUUtilization. Tools in your operating system can show a different percentage than CloudWatch due to factors such as legacy device simulation, configuration of non-legacy devices, interrupt-heavy workloads, live migration, and live update.

## Widgets Array Structure

Each widget of any type can have the following properties.

type
The type of widget.

Valid Values: metric | text | log | alarm

Type: String

Required: Yes

x
The horizontal position of the widget on the 24-column dashboard grid. The default is the next available position.

Valid Values: 0–23

Type: Integer

Required: Yes, if y is specified. Otherwise not required.

y
The vertical position of the widget on the 24-column dashboard grid. The default is the next available position.

Valid Values: Any integer, 0 or higher.

Type: Integer

Required: Yes, if x is specified. Otherwise not required.

width
The width of the widget in grid units (in a 24-column grid). The default is 6.

Valid Values: 1–24

Type: Integer

Required: No

height
The height of the widget in grid units. The default is 6.

Valid Values: 1–1000

Type: Integer

Required: No

properties
The detailed properties of the widget, which differ depending on the widget type. For more information about the format of properties

----------------------------------------------------------------------------------------------------------------------------------------

## How to simulate CPU overload ?

For Amazon Linux 2023 VM, follow the below steps

sudo yum update
sudo yum install stress
Generate CPU stress: stress --cpu <number-of-threads>

Generate memory stress: stress --vm <number-of-threads> --vm-bytes <memory-size>

Generate I/O stress: stress --io <number-of-threads>

Generate disk stress: stress --hdd <number-of-threads>

* Launch the stress test in background (for 800 seconds in that example)

sudo stress --cpu 8 --timeout 800 &

* Check the stress test processes with the top command

top

* Manually stop the stress test (if needed)

sudo killall stress

--------------------------------------------------------------------------------------------------------------------------------------
## What is SQS ? 

Amazon Simple Queue Service (SQS) is a fully managed message queuing service provided by AWS. With SQS, messages can be stored and retrieved from queues ensuring reliable delivery.

It enables reliable and scalable communication between various components of distributed systems, allowing decoupling of applications and asynchronous processing of messages.

Some alternatives to SQS include RabbitMQ and Apache Kafka.

-------------------------------------------------------------------------------------------------------------------------------------

