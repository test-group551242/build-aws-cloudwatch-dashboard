#ASG Launch template 
resource "aws_launch_template" "my-launch-templ" {

  name_prefix = "my-launch-template"
  image_id = var.ami_ec2
  instance_type = "t3.micro"
  user_data = filebase64("user_data.sh")

  #iam_instance_profile {name = aws_iam_instance_profile.my_instance_profile.name}

  network_interfaces {
    associate_public_ip_address = false
    security_groups = ["${aws_security_group.sg-asg.id}"]
  }
  
  #key_name = "my-key"

  lifecycle {
    create_before_destroy = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "ASG-instances"
    }
  }
}

#launch asg that spans both AZs 
resource "aws_autoscaling_group" "my-asg" {

  name = "my-asg"

  desired_capacity = 1
  max_size = 2
  min_size = 1

  target_group_arns = ["${aws_lb_target_group.my-alb-target.arn}"] #connect to the ALB

  vpc_zone_identifier = ["${element(module.my-vpc.private_subnets, 0)}" , "${element(module.my-vpc.private_subnets, 1)}"]  #asg subnets 
  
  launch_template {
    id = aws_launch_template.my-launch-templ.id
    version = aws_launch_template.my-launch-templ.latest_version
  }

}

resource "aws_autoscaling_policy" "cpu_scaling_policy" {
  name                   = "ASG-cpu-scaling-policy"
  policy_type            = "TargetTrackingScaling"
  estimated_instance_warmup = 30
  autoscaling_group_name = aws_autoscaling_group.my-asg.name

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 40
  }
}


resource "aws_autoscaling_attachment" "application_asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.my-asg.name
  lb_target_group_arn    = aws_lb_target_group.my-alb-target.arn
}

