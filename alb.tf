
resource "aws_lb" "my-alb" {
  name = "my-alb"
  internal = false 
  load_balancer_type = "application"
  security_groups = [ aws_security_group.sg-alb.id ]
  subnets = module.my-vpc.public_subnets   #subnets to attach to the LB
  depends_on = [ module.my-vpc.igw_id ]

}

resource "aws_lb_target_group" "my-alb-target" {
  name = "my-alb-targets"
  port = 80
  protocol = "HTTP"
  vpc_id = module.my-vpc.vpc_id
  
  target_type = "instance"
  load_balancing_algorithm_type = "round_robin"
  
  health_check {
    path                = "/"
    port                = 80
    protocol            = "HTTP"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "my-alb-front-end" {
  load_balancer_arn = aws_lb.my-alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-alb-target.arn
  }
}

